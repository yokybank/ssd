import os,cv2,sys,shutil
from xml.dom.minidom import Document
def writexml(filename,saveimg,bboxes,xmlpath):
    doc=Document()
    annotation=doc.createElement('annotation')
    doc.appendChild(annotation)
    folder=doc.createElement('folder')
    folder_name=doc.createTextNode('widerface')
    folder.appendChild(folder_name)
    annotation.appendChild(folder)
    filenamenode=doc.createElement('filename')
    filename_name=doc.createTextNode(filename)
    filenamenode.appendChild(filename_name)
    annotation.appendChild(filenamenode)
    source=doc.createElement('source')
    annotation.appendChild(source)
    database=doc.createElement('database')
    database.appendChild(doc.createTextNode('wider face database'))
    source.appendChild(database)
    annotation_s=doc.createElement('annotation')
    annotation_s.appendChild(doc.createTextNode('PASCAL VOC2007'))
    source.appendChild(annotation_s)
    image=doc.createElement('image')
    image.appendChild(doc.createTextNode('flickr'))
    source.appendChild(image)
    flickrid=doc.createElement('flickrid')
    flickrid.appendChild(doc.createTextNode('-1'))
    source.appendChild(flickrid)
    owner=doc.createElement('owner')
    annotation.appendChild(owner)
    flickrid_o=doc.createElement('flickrid')
    flickrid_o.appendChild(doc.createTextNode('yanyu'))
    owner.appendChild(flickrid_o)
    size_node=doc.createElement('size')
    annotation.appendChild(size_node)
    width=doc.createElement('width')
    width.appendChild(doc.createTextNode(str(saveimg.shape[1])))
    height = doc.createElement('height')
    height.appendChild(doc.createTextNode(str(saveimg.shape[0])))
    length = doc.createElement('length')
    length.appendChild(doc.createTextNode(str(saveimg.shape[2])))
    size_node.appendChild(width)
    size_node.appendChild(height)
    size_node.appendChild(length)
    segmented=doc.createElement('segmented')
    segmented.appendChild(doc.createTextNode('0'))
    annotation.appendChild(segmented)
    for i in range(len(bboxes)):
        bbox=bboxes[i]
        objects=doc.createElement('object')
        annotation.appendChild(objects)
        object_name=doc.createElement('name')
        object_name.appendChild(doc.createTextNode('face'))
        object_pose=doc.createElement('pose')
        object_pose.appendChild(doc.createTextNode('Right'))
        object_truncated=doc.createElement('truncated')
        object_truncated.appendChild(doc.createTextNode('0'))
        object_diff = doc.createElement('difficult')
        object_diff.appendChild(doc.createTextNode('0'))
        object_bndbox = doc.createElement('bndbox')
        object_xmin=doc.createElement('xmin')
        object_xmin.appendChild(doc.createTextNode(str(bbox[0])))
        object_ymin = doc.createElement('ymin')
        object_ymin.appendChild(doc.createTextNode(str(bbox[1])))
        object_xmax = doc.createElement('xmax')
        object_xmax.appendChild(doc.createTextNode(str(bbox[0]+bbox[2])))
        object_ymax = doc.createElement('ymax')
        object_ymax.appendChild(doc.createTextNode(str(bbox[1]+bbox[3])))
        object_bndbox.appendChild(object_xmin)
        object_bndbox.appendChild(object_ymin)
        object_bndbox.appendChild(object_xmax)
        object_bndbox.appendChild(object_ymax)
        objects.appendChild(object_bndbox)
        object_diff.appendChild(doc.createTextNode('0'))
    f=open(xmlpath,"w")
    f.write(doc.toprettyxml(indent=''))
    f.close()
root_dir="/home/jjuv/WIDERFaceDatasets"

def convertimgset(img_set):
    imgdir=root_dir+"/WIDER_"+img_set+"/images/"
    gtfilepath=root_dir+"/wider_face_split/wider_face_"+img_set+"_bbx_gt.txt"
    if not os.path.exists(root_dir+"/ImageSets" ):
        os.mkdir(root_dir+"/ImageSets")
    if not os.path.exists(root_dir+"/ImageSets/Main/"):
        os.mkdir(root_dir+"/ImageSets/Main/")
    fwriter = open(root_dir + "/ImageSets/Main/" + img_set + ".txt", 'w')
    index=0
    with open(gtfilepath,'r') as gtfiles:
        while(True):
            filename=gtfiles.readline()[:-1]
            print(filename)
            if(filename==""):
                continue
            imgpath=imgdir+filename
            img=cv2.imread(imgpath)

            if img is None:
                continue
            numbbox=int(gtfiles.readline())
            bboxes=[]
            for i in range(numbbox):
                line=gtfiles.readline()
                lines=line.split(" ")
                lines=lines[0:4]
                bbox=(int(lines[0]),int(lines[1]),int(lines[2]),int(lines[3]))
                bboxes.append(bbox)
            filename=filename.replace('/','_')

            if len(bboxes)==0:
                print("no face")
                continue
            if not os.path.exists(root_dir + "/JPEGImages"):
                os.mkdir(root_dir + "/JPEGImages")
            if not os.path.exists(root_dir + "/Annotations"):
                os.mkdir(root_dir + "/Annotations")
            cv2.imwrite("{}/JPEGImages/{}".format(root_dir,filename),img)
            fwriter.write(filename.split(".")[0]+"\n")
            xmlpath="{}/Annotations/{}.xml".format(root_dir,filename.split(".")[0])
            writexml(filename,img,bboxes,xmlpath)
            print("success number is",index)
            index+=1
        fwriter.close()
if __name__=="__main__":
    img_sets=["train","val"]
    for img_set in img_sets:
        convertimgset(img_set)
    shutil.move(root_dir+"/ImageSets/Main/"+"train.txt",root_dir+"/ImageSets/Main/"+"trainval.txt")
    shutil.move(root_dir + "/ImageSets/Main/" + "val.txt", root_dir + "/ImageSets/Main/" + "test.txt")
